# Imagen docker base inicial
FROM node:latest

# Crear directorio de trabajo del contenedor
WORKDIR /docker-api

# Copiar archivos del proyecto en directorio de Docker
ADD . /docker-api

# Instalar las dependencias del proyecto en Produccion
#RUN npm install --production

# Puerto donde exponemos nuestro contenedor (mismo que definimos en nuestra API)
EXPOSE 3000

# Lanzar la aplicacion (appe.js)
CMD ["npm", "start"]
