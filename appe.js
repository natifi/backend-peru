var express = require('express');
var requestJson = require('request-json');
var bodyParser = require('body-parser');
var app = express();
var port = process.env.PORT || 3000;
var URLbase = '/apiperu/v1/';

//CONEXION A mLAB
var urlMlabRaiz = "https://api.mlab.com/api/1/databases/apiperu/collections/";
var apiKey = "apiKey=56bfFyBmAHRgG2H8IqYeTft8IXoQb8_b";
var clienteMlab;

app.use(bodyParser.json());
clienteMlab = requestJson.createClient(urlMlabRaiz);

//OBTIENE LISTADO DE USUARIOS
app.get(URLbase + 'users', function(req, res) {
    let queryString = 'f={_id:0,password:0}&l=10&';
    clienteMlab.get('user?' + queryString + apiKey,
    function(err, resM, body) {
      var response={};
      if (err) {
        response = {"msg":"error obteniendo usuario"};
        res.status(500);
      }else {
        if(body.length >0){
          response=body;
        }else{
          response = {"msg":"ningùn elemento usuario"};
          res.status(404);
        }
      }
      res.send(response);
    });
});

//OBTIENE LISTADO DE MOVIMIENTOS
app.get(URLbase + 'accounts', function(req, res) {
    let queryString = 'f={_id:0}&l=10&';
    clienteMlab.get('account?' + queryString + apiKey,
    function(err, resM, body) {
      var response={};
      if (err) {
        response = {"msg":"error obteniendo usuario"};
        res.status(500);
      }else {
        if(body.length >0){
          response=body;
        }else{
          response = {"msg":"ningùn elemento usuario"};
          res.status(404);
        }
      }
      res.send(response);
    });
});

//CONSULTA USUARIO ESPECIFICO
app.get(URLbase + 'users/:id', function(req, res) {
    var id = req.params.id;
    let queryString = 'q={"user_id":' + id + '}&f={_id:0 }&l=1&';
    clienteMlab.get('user?' + queryString + apiKey,
    function(err, resM, body) {
      var response={};
      if (err) {
        response = {"msg":"error obteniendo usuario"};
        res.status(500);
      }else {
        if(body.length >0){
          response=body;
        }else{
          response = {"msg":"ningùn elemento usuario"};
          res.status(404);
        }
      }
      res.send(response);
    });
});


//AGREGA UN NUEVO USUARIO A LA BD
app.post(URLbase + 'users/',
  function(req, res){
    clienteMlab.get('user?' + apiKey,
      function(error, respuestaMLab, body){
        newID = body.length + 1;
        var newUser = {
          "user_id" : newID,
          "first_name" : req.body.first_name,
          "last_name" : req.body.last_name,
          "email" : req.body.email,
          "password" : req.body.password
        };
        clienteMlab.post(urlMlabRaiz + "user?" + apiKey, newUser,
          function(error, respuestaMLab, body){
            res.send(body);
          });
      });
  });

//ELIMINA USUARIO DE LA BD
app.delete(URLbase + "users/:id",
  function(req, res){
    var id = req.params.id;
    var queryStringID = 'q={"user_id":' + id + '}&';
    clienteMlab.get('user?' +  queryStringID + apiKey,
      function(error, respuestaMLab, body){
        var respuesta = body[0];
        clienteMlab.delete(urlMlabRaiz + "user/" + respuesta._id.$oid +'?'+ apiKey,
          function(error, respuestaMLab,body){
              res.send(body);
        });
      });
  });

//OBTIENE TODAS LAS CUENTAS DE UN USUARIO
app.get(URLbase + 'accounts/:id', function(req, res) {
    var id = req.params.id;
    let queryString = 'q={"user_id":' + id + '}&f={_id:0,movement_id:0,date:0,amount:0,type:0 }&l=10&';
    clienteMlab.get('account?' + queryString + apiKey,
    function(err, resM, body) {
      var response={};
      if (err) {
        response = {"msg":"error obteniendo usuario"};
        res.status(500);
      }else {
        if(body.length >0){
          response=body;
        }else{
          response = {"msg":"ningùn elemento usuario"};
          res.status(404);
        }
      }
      res.send(response);
    });
});

//OBTIENE TODOS LOS MOVIMIENTOS DE UN USUARIO
app.get(URLbase + 'movements/:id/', function(req, res) {
    var id = req.params.id;
    let queryString = 'q={"user_id":' + id +'}&f={_id:0 }&l=10&';
    clienteMlab.get('account?' + queryString + apiKey,
    function(err, resM, body) {
      var response={};
      if (err) {
        response = {"msg":"error obteniendo usuario"};
        res.status(500);
      }else {
        if(body.length >0){
          response=body;
        }else{
          response = {"msg":"ningùn elemento usuario"};
          res.status(404);
        }
      }
      res.send(response);
    });
});

//OBTIENE LISTADO DE MOVIMIENTOS FILTRANDO POR CUENTA Y USUARIO
app.get(URLbase + 'movements/:id/:account', function(req, res) {
    var id = req.params.id;
    var account = req.params.account;
    let queryString = 'q={"user_id":' + id + ',"account_id":'+ account+'}&f={_id:0 }&l=10&';
    clienteMlab.get('account?' + queryString + apiKey,
    function(err, resM, body) {
      var response={};
      if (err) {
        response = {"msg":"error obteniendo usuario"};
        res.status(500);
      }else {
        if(body.length >0){
          response=body;
        }else{
          response = {"msg":"ningùn elemento usuario"};
          res.status(404);
        }
      }
      res.send(response);
    });
});

//OBTIENE LISTADO DE MOVIMIENTOS FILTRANDO POR USUARIO Y TIPO DE MOVIMIENTO
app.get(URLbase + 'movements/:id/type/:type', function(req, res) {
    var id = req.params.id;
    var type = req.params.type;
    let queryString = 'q={"user_id":' + id +
                          ',"type":' + type + '}&f={_id:0 }&l=10&';
    clienteMlab.get('account?' + queryString + apiKey,
    function(err, resM, body) {
      var response={};
      if (err) {
        response = {"msg":"error obteniendo usuario"};
        res.status(500);
      }else {
        if(body.length >0){
          response=body;
        }else{
          response = {"msg":"ningùn elemento usuario"};
          res.status(404);
        }
      }
      res.send(response);
    });
});

//OBTIENE LISTADO DE MOVIMIENTOS FILTRANDO POR USUARIO, CUENTA Y TIPO DE MOVIMIENTO
app.get(URLbase + 'movements/:id/:account/type/:type', function(req, res) {
    var id = req.params.id;
    var account = req.params.account;
    var type = req.params.type;
    let queryString = 'q={"user_id":' + id + ',"account_id":'+ account+
                          ',"type":' + type + '}&f={_id:0 }&l=10&';
    clienteMlab.get('account?' + queryString + apiKey,
    function(err, resM, body) {
      var response={};
      if (err) {
        response = {"msg":"error obteniendo usuario"};
        res.status(500);
      }else {
        if(body.length >0){
          response=body;
        }else{
          response = {"msg":"ningùn elemento usuario"};
          res.status(404);
        }
      }
      res.send(response);
    });
});

// CREA UN NUEVO MOVIMIENTO EN LA BD
app.post(URLbase + 'movements/',
  function(req, res){
    clienteMlab.get('account?' + apiKey,
      function(error, respuestaMLab, body){
        newID = body.length + 1;
        var newMovement = {
          "movement_id" : newID,
          "user_id" : req.body.user_id,
          "account_id" : req.body.account_id,
          "amount" : req.body.amount,
          "type" : req.body.type,
          "date" : req.body.date
        };
        clienteMlab.post(urlMlabRaiz + "account?" + apiKey, newMovement,
          function(error, respuestaMLab, body){
            res.send(body);
          });
      });
  });

//LOGIN
app.post(URLbase + 'login/', function(req, res) {
    var email = req.body.email;
    var password = req.body.password;
    var query = 'q={"email":"' + email + '","password":"' + password + '"}' + '&f={_id:0}+&l=1&';
    clienteMlab.get('user?' + query + apiKey, function(err, resM, body) {
      if (!err) {
        if (body.length == 1) // Login ok
        {
            var cambio = '{"$set":{"logged":true}}'
            clienteMlab.put('user?q={"user_id": ' + body[0].user_id + '}&' + apiKey, JSON.parse(cambio), function(errP, resP, bodyP) {
            res.send({"login":"ok", "user_id":body[0].user_id, "first_name":body[0].first_name, "last_name":body[0].last_name })
          })

        }
        else {
            res.status(404).send('Usuario no encontrado')
        }
      }
    })
})

//LOGOUT
app.post(URLbase + 'logout/', function(req, res) {
    var email = req.body.email;
    var query = 'q={"email":"' + email + '","logged":true}'  + '&l=1&';
    clienteMlab.get('user?' + query + apiKey, function(err, resM, body) {
    if (!err) {
        if (body.length == 1) // Login ok
          {
            var cambio = '{"$unset":{"logged":true}}'
            clienteMlab.put('user?q={"user_id": ' + body[0].user_id + '}&' + apiKey, JSON.parse(cambio), function(errP, resP, bodyP) {
            res.send({"logout":"ok", "user_id":body[0].user_id, "first_name":body[0].first_name, "last_name":body[0].last_name })
            })
           }
        else {
            res.status(404).send('Usuario no logueado')
         }
        }
      })
})



//GET CURRENCY CONVERSION
//RETORNA EL MONTO INGRESADO EN LA MONEDA SOLICITADA
//RETORNA EL TIPO DE CAMBIO EMPLEADO
app.post(URLbase + 'currency', function(req, res) {
    //OBTENCION TIPO DE CAMBIO
    var URLcurrency= "http://free.currencyconverterapi.com/api/v6/";
    var clienteCurrency = requestJson.createClient(URLcurrency);
    var fromCurrency = req.body.fromCurrency;
    var toCurrency = req.body.toCurrency;
    var amount = req.body.amount;
    var query = fromCurrency + '_' + toCurrency;
    clienteCurrency.get('convert?q=' + query +  '&compact=y' ,
    function(err, resM, body) {
      var response={};
      if (err) {
                response = {"msg":"error obteniendo tipo cambio"};
                res.status(500);
      }else {
        if(body.success ="true"){
            var val = body[query].val;
            if (val)
              var val_converted = val * amount;
              response = {
                  "tipo_cambio" : val,
                  "val_converted" : val_converted
              }
          }else{
              response = {"msg":"no se obtuvo respuestaMLab"};
              res.status(404);
          }
        }
        res.send(response);
      });
});


app.listen(3000);
